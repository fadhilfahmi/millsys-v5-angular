import { Component, ChangeDetectorRef } from '@angular/core';
import { AppSidebarService } from './app-sidebar.service';
import { Module } from './module';

@Component({
  selector: '[app-sidebar]',
  templateUrl: './app-sidebar.component.html'
})
export class AppSidebar {

  modules: Module[];

  constructor(private appsidebarService: AppSidebarService,private chRef: ChangeDetectorRef){}

  ngOnInit() {

    
    this.appsidebarService.getModule()
    .subscribe( data => {
      this.modules = data;
      
    });

    this.chRef.detectChanges();
  }
}
