import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditChartofaccountComponent } from './edit-chartofaccount.component';

describe('EditChartofaccountComponent', () => {
  let component: EditChartofaccountComponent;
  let fixture: ComponentFixture<EditChartofaccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditChartofaccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditChartofaccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
