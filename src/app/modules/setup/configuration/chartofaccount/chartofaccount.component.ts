import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { Chartofaccount } from './chartofaccount';
import { ChartofaccountService } from './chartofaccount.service';

@Component({
  selector: 'app-chartofaccount',
  templateUrl: './chartofaccount.component.html',
  styleUrls: ['./chartofaccount.component.css']
})
export class ChartofaccountComponent implements OnInit {

  chartofaccount: Chartofaccount[];
  dataTable: any;

  constructor(private http: HttpClient, private chRef: ChangeDetectorRef){}

  ngOnInit(){

    this.http.get('http://localhost:8080/api/setup/configuration/coa/datatable')
      .subscribe((data: any[]) => {
        this.chartofaccount = data;
        this.chRef.detectChanges();
        const table:any = $('table');
        this.dataTable = table.DataTable({
          responsive: true,
          "bProcessing": true,
          "aaSorting": [[0, "asc"]],
          "aoColumns": [
            {"sWidth": "10%", "bSearchable": true},
            {"sWidth": "35%", "bSearchable": true},
            {"sWidth": "35%", "bSearchable": true},
            {"sWidth": "10%", "bSearchable": true},
            {"sWidth": "10%"}
          ]/*,
          "aoColumnDefs": [{
                  "aTargets": [4],
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {

                      //var a = '<button type="button" class="btn btn-primary btn-xs">Primary</button>';
                      var a = '';

                      $(nTd).empty();
                      $(nTd).attr("class", 'btn-group');
                      $(nTd).prepend(a);
                  }

              }]*/
        });

      });

    
  }

  goToView(this){
    console.log(this.chartofaccount[0].code);
   // console.log($(this).attr('id'));
  }

}
