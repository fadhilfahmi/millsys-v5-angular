import { TestBed, inject } from '@angular/core/testing';

import { ChartofaccountService } from './chartofaccount.service';

describe('ChartofaccountService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChartofaccountService]
    });
  });

  it('should be created', inject([ChartofaccountService], (service: ChartofaccountService) => {
    expect(service).toBeTruthy();
  }));
});
