import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewChartofaccountComponent } from './view-chartofaccount.component';

describe('ViewChartofaccountComponent', () => {
  let component: ViewChartofaccountComponent;
  let fixture: ComponentFixture<ViewChartofaccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewChartofaccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewChartofaccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
